<?php

$app->get('/ping', function () use ($app) {
    return json_encode([env('APP_NAME') => true]);
});


$app->post('/users', 'UsersController@create');
$app->get('/users/{id:[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{8}}', 'UsersController@get');
$app->get('/users', 'UsersController@getAll');
$app->patch('/users', 'UsersController@update');

$app->post('/activities', 'ActivitiesController@create');
$app->get('/activities/{id:[0-9]{1,10}}', 'ActivitiesController@get');
$app->get('/activities', 'ActivitiesController@getAll');

$app->post('/ratings', 'RatingsController@create');
$app->get('/ratings/{id:[0-9]{1,10}}', 'RatingsController@get');
$app->get('/ratings', 'RatingsController@getAll');

$app->post('/reviews', 'ReviewsController@create');
$app->get('/reviews/{id:[0-9]{1,10}}', 'ReviewsController@get');
$app->get('/reviews', 'ReviewsController@getAll');


