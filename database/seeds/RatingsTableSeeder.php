<?php

use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ratings')->insert([
            'score' => 1,
            'description' => 'This is bad'
        ]);
        DB::table('ratings')->insert([
            'score' => 2,
            'description' => 'This is still bad'
        ]);
        DB::table('ratings')->insert([
            'score' => 3,
            'description' => 'This is better'
        ]);
        DB::table('ratings')->insert([
            'score' => 4,
            'description' => 'This is good'
        ]);
        DB::table('ratings')->insert([
            'score' => 5,
            'description' => 'Perfect'
        ]);
    }
}
