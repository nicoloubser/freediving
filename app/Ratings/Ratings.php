<?php

namespace App\Ratings;

use App\Models\RatingsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Ratings
{
    protected $ratings;

    public function __construct(RatingsModel $ratings )
    {
        $this->ratings = $ratings;
    }

    /**
     * create
     *
     * @param Request $request
     *
     * @return bool
     */
    public function create(Request $request)
    {
        try {
            $this->ratings->name = $request->get('score');
            $this->ratings->description = $request->get('description');
            $this->ratings->save();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Returns a single user
     *
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        try {
            $result = $this->ratings->where('id', $id)->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }

    /**
     * Returns a list of users.
     *
     * @return mixed
     */
    public function getAll()
    {
        try {
            $result = $this->ratings->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }
}


