<?php

namespace App\Users;

use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class Users
{
    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    /**
     * create
     *
     * @param Request $request
     *
     * @return bool
     */
    public function create(Request $request)
    {
        try {
            $this->user->uuid = Uuid::generate();
            $this->user->email = $request->get('email');
            $this->user->password = Hash::make($request->get('password'));
            $this->user->first_name = $request->get('first_name');
            $this->user->last_name = $request->get('last_name');
            $this->user->save();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Returns a single user
     *
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        try {
            $result = $this->user->where('uuid', $id)->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }

    /**
     * Returns a list of users.
     *
     * @return mixed
     */
    public function getAll()
    {
        try {
            $result = $this->user->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }
}


