<?php

namespace App\Http\Controllers;

use App\Activities\Activities;
use App\Validators\ActivitiesCreateValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ActivitiesController extends Controller
{
    /**
     * @var Activities
     */
    protected $activities;
    /**
     * @var Request
     */
    protected $request;
    
    public function __construct(Request $request, Activities $activities)
    {
        $this->request = $request;
        $this->activities = $activities;
    }

    /**
     * Creates a new activities.
     *
     * @param ActivitiesCreateValidation $activitiesValidation
     *
     * @return JsonResponse
     */
    public function create(ActivitiesCreateValidation $activitiesValidation)
    {
        // Input validation
        if (($result = $activitiesValidation->isValid($this->request)) !== true) {
            return $this->response(['result' => $result], Response::HTTP_BAD_REQUEST);
        }

        list($response, $code) = $this->formatResponse($this->activities->create($this->request));

        return $this->response($response, $code);
    }

    /**
     * Retrieves a activities based on an id
     *
     * @param $id uuid to id a activities
     *
     * @return JsonResponse
     */
    public function get($id)
    {
        $result = $this->activities->get($id);
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }

    /**
     * Retrieves a user based on uuid
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $result = $this->activities->getAll();
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }
   
}