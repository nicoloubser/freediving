<?php

namespace App\Http\Controllers;

use App\Ratings\Ratings;
use App\Validators\RatingsCreateValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RatingsController extends Controller
{
    /**
     * @var Ratings
     */
    protected $ratings;
    /**
     * @var Request
     */
    protected $request;
    
    public function __construct(Request $request, Ratings $ratings)
    {
        $this->request = $request;
        $this->ratings = $ratings;
    }

    /**
     * Creates a new activities.
     *
     * @param RatingsCreateValidation $ratingsValidation
     *
     * @return JsonResponse
     */
    public function create(RatingsCreateValidation $ratingsValidation)
    {
        // Input validation
        if (($result = $ratingsValidation->isValid($this->request)) !== true) {
            return $this->response(['result' => $result], Response::HTTP_BAD_REQUEST);
        }

        list($response, $code) = $this->formatResponse($this->ratings->create($this->request));

        return $this->response($response, $code);
    }

    /**
     * Retrieves a activities based on an id
     *
     * @param $id uuid to id a activities
     *
     * @return JsonResponse
     */
    public function get($id)
    {
        $result = $this->ratings->get($id);
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }

    /**
     * Retrieves a user based on uuid
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $result = $this->ratings->getAll();
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }
}