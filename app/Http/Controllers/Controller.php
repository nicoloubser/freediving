<?php

namespace App\Http\Controllers;

use App\Traits\StdResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use StdResponse;
    
    /**
     * formatResponse
     *
     * @param $result
     * @return array
     */
    protected function formatResponse($result)
    {
        if ($result === false) {
            $response = ['result' => 'failed'];
            return [$response, Response::HTTP_BAD_REQUEST];
        } else {
            $response = ['result' => 'success'];
            return [$response, Response::HTTP_OK];
        }
    }

    /**
     * Standard formatting for gets
     * 
     * @param $result
     * 
     * @return Response
     */
    protected function formatGetResults($result)
    {
        if ($result === false) {
            $functionResponse = response(['result' => 'failed'], Response::HTTP_BAD_REQUEST);
            return $functionResponse;
        } elseif (empty($result)) {
            $functionResponse = response(['result' => $result], Response::HTTP_NO_CONTENT);
            return $functionResponse;
        } else {
            $functionResponse = response(['result' => $result], Response::HTTP_OK);
            return $functionResponse;
        }
    }
}
