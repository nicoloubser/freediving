<?php

namespace App\Http\Controllers;

use App\Reviews\Reviews;
use App\Validators\ReviewsCreateValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReviewsController extends Controller
{
    /**
     * @var Reviews
     */
    protected $reviews;
    /**
     * @var Request
     */
    protected $request;
    
    public function __construct(Request $request, Reviews $reviews)
    {
        $this->request = $request;
        $this->reviews = $reviews;
    }

    /**
     * Creates a new activities.
     *
     * @param ReviewsCreateValidation $reviewsValidation
     *
     * @return JsonResponse
     */
    public function create(ReviewsCreateValidation $reviewsValidation)
    {
        // Input validation
        if (($result = $reviewsValidation->isValid($this->request)) !== true) {
            return $this->response(['result' => $result], Response::HTTP_BAD_REQUEST);
        }

        list($response, $code) = $this->formatResponse($this->reviews->create($this->request));

        return $this->response($response, $code);
    }

    /**
     * Retrieves a activities based on an id
     *
     * @param $id uuid to id a activities
     *
     * @return JsonResponse
     */
    public function get($id)
    {
        $result = $this->reviews->get($id);
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }

    /**
     * Retrieves a user based on uuid
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $result = $this->reviews->getAll();
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }
}