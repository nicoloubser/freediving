<?php

namespace App\Http\Controllers;

use App\Users\Users;
use App\Validators\UsersCreateValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsersController extends Controller
{
    /**
     * @var Users
     */
    protected $user;
    /**
     * @var Request
     */
    protected $request;
    
    public function __construct(Request $request, Users $user)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Creates a new user.
     * 
     * @return JsonResponse
     */
    public function create(UsersCreateValidation $usersValidation)
    {
        // Input validation
        if (($result = $usersValidation->isValid($this->request)) !== true) {
            return $this->response(['result' => $result], Response::HTTP_BAD_REQUEST);
        }

        list($response, $code) = $this->formatResponse($this->user->create($this->request));

        return $this->response($response, $code);
    }

    /**
     * Retrieves a user based on uuid
     *
     * @param $id uuid to id a user
     *
     * @return JsonResponse
     */
    public function get($id)
    {
        $result = $this->user->get($id);
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }

    /**
     * Retrieves a user based on uuid
     *
     * @param $id uuid to id a user
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $result = $this->user->getAll();
        $functionResponse = $this->formatGetResults($result);
        return $functionResponse;
    }

}