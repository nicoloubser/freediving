<?php

namespace App\Activities;

use App\Models\ActivitiesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Activities
{
    protected $activities;

    public function __construct(ActivitiesModel $activities )
    {
        $this->activities = $activities ;
    }

    /**
     * create
     *
     * @param Request $request
     *
     * @return bool
     */
    public function create(Request $request)
    {
        try {
            $this->activities->name = $request->get('name');
            $this->activities->description = $request->get('description');
            $this->activities->save();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Returns a single user
     *
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        try {
            $result = $this->activities->where('id', $id)->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }

    /**
     * Returns a list of users.
     *
     * @return mixed
     */
    public function getAll()
    {
        try {
            $result = $this->activities->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }
}


