<?php

namespace App\Traits;

/**
 * Trait since it may be used in difference concerns
 */
trait StdResponse
{
    public function response($response, $code = 200, $message = '')
    {
        $return = [
            $response
        ];

        return response()->json($return, $code);
    }
}