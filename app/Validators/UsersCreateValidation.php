<?php

namespace App\Validators;

class UsersCreateValidation extends CustomValidation
{
    protected $rules = [
        'email' => 'unique:mysql.users,email|required|email|max:255',
        'password' => 'required|string|max:255',
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255'
    ];    
}