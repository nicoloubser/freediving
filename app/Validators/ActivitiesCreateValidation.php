<?php

namespace App\Validators;

class ActivitiesCreateValidation extends CustomValidation
{
    protected $rules = [
        'name' => 'unique:mysql.activities,name|required|string|max:255',
        'description' => 'required|string'
    ];    
}