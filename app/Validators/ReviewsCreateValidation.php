<?php

namespace App\Validators;

class ReviewsCreateValidation extends CustomValidation
{
    protected $rules = [
        'user_id' => 'required|integer',
        'location_id' => 'required|integer',
        'rating_id' => 'required|integer',
        'sport_id' => 'required|integer',
        'comment' => 'required|string'
    ];    
}
