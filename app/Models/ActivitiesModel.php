<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivitiesModel extends Model
{
    protected $table = 'activities';
}