<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewsModel extends Model
{
    protected $table = 'reviews';

    public function activity()
    {
        return $this->hasOne('App\Models\ActivitiesModel', 'id', 'sport_id');
    }

    public function ratings()
    {
        return $this->hasMany('App\Models\RatingsModel', 'id', 'rating_id');
    }
}