<?php

namespace App\Reviews;

use App\Models\ReviewsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Reviews
{
    protected $reviews;

    public function __construct(ReviewsModel $reviews )
    {
        $this->reviews = $reviews;
    }

    /**
     * create
     *
     * @param Request $request
     *
     * @return bool
     */
    public function create(Request $request)
    {
        try {
            $this->reviews->user_id = $request->get('user_id');
            $this->reviews->location_id = $request->get('location_id');
            $this->reviews->rating_id = $request->get('rating_id');
            $this->reviews->sport_id = $request->get('sport_id');
            $this->reviews->comment = $request->get('comment');
            $this->reviews->save();

        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Returns a single rating
     *
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        try {
            $result = $this->reviews->with('activity')->with('ratings')->find($id);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }

    /**
     * Returns a list of reviews.
     *
     * @return mixed
     */
    public function getAll()
    {
        try {
            $result = $this->reviews->get();
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return false;
        }
        return $result;
    }
}


